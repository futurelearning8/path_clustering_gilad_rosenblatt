# Path Clustering

This is the path clustering detection exercise for FL8 week 3 day 4 by Gilad Rosenblatt.

## Instructions

### Dependencies

The clustering code makes use of the `hdbscan` package for hierarchical density-based scan clustering (no included in `sklearn`). This may require to install Microsoft Visual Studio Developer Tools (for C++ support), and uninstalling-and-reinstalling `numpy` after installing [hdbscan](https://pypi.org/project/hdbscan/) (`pip install hdbscan`).

### Run

To run path clustering execute `main.py` in the `code` folder. Much of the underlying path logic is found in the Path class in `paths.py`.

### Data

Data is not included in this repo. There should be three folders inside a `data`I directory.

1. A symbolic link to a directory `mot_benchmark` that includes preprocessed detection bounding boxes for the MOT benchmark dataset. The image corresponding to the first frame of the `PETS09-S2L1` sequence is read from here by `main.py`.

2. An `output` folder (created automatically) to which the SORT algorithm writes the tracker benchmarks when `sort.py` is executed.

3. A `cluster` folder from which `main.py` reads handpicked SORT output files for the `PETS09-S2L1` train set sequence.

## Results

Feature-matrix-based HDBSCAN clustering:

Lenient parameters (`min_cluster_size=3`, `min_samples=1`)

![approch2](/images/clustering_features_HDBSCAN.png?raw=true)

Restrictive hyperparameters (`min_cluster_size=6`, `min_samples=2`)

![approch2_aggressive](/images/clustering_features_HDBSCAN_mcs6_ms2_combined.png?raw=true)

Distance-matrix-based HDBSCAN clustering:

![approch1](/images/clustering_distances_HDBSCAN.png?raw=true)

## License

[WTFPL](http://www.wtfpl.net/)
