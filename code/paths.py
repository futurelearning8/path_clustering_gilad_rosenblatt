import random
import itertools
import cv2
import numpy as np
from scipy.spatial.distance import directed_hausdorff
from scipy import interpolate


class Path:
    """A path tracked by a multi-object tracker across a video feed."""

    # Unique path id (instance counter starting from 0).
    _counter = -1

    # Interpolation constant: how many points a path will be interpolated (featurized) into.
    _num_interpolations = 100

    def __init__(self, tracker_id, points):
        """
        Initialize a path object from the given sequence of points.

        :param int tracker_id: ID of the tracker that produces the path.
        :param np.ndarray points: the sequence of points that comprise the path (x_centroid, y_centroid, frame_number).
        """

        # Generate instance index as unique path id.
        self._id = Path._generate_id()

        # Save tracker id.
        self.tracker_id = tracker_id

        # Save numpy array of 3D points: (x_centroid, y_centroid, frame_number) x number_of_points [row x column].
        self.points = points

        # Calculate some frame stats.
        self.number_of_points = self.points.shape[0]
        self.first_frame = np.min(self.points[:, 2]).astype(int)
        self.last_frame = np.max(self.points[:, 2]).astype(int)
        self.lifespan = 1 + self.last_frame - self.first_frame
        self.singlet = self.lifespan == 1

        # Normalize "time" to be relative to the path's start frame (allows time-invariant path comparison).
        self.points[:, 2] = self.points[:, 2] - self.first_frame

        # Featurize by interpolating x-y pixel coordinates at path-distance percentiles (mark invalid path if failed).
        self.valid = self.points.shape[0] > 3  # Cubic spline needs to calculate 2 derivatives.
        self.points_interp = None
        if self.valid:
            self._featurize()  # Fills points_interp.

    def __repr__(self):
        s = f"Path id {self.id}: " \
            f"frames {self.first_frame} --> {self.last_frame} ({self.lifespan}), " \
            f"tracker {self.tracker_id}, {self.number_of_points} points)"
        return s

    def draw_raw(self, image, color):
        """
        Draw this path on an input image using the raw tracker-supplied points that define the path.

        :param image: image to draw this path on (using opencv).
        :param tuple color: a color spec (3 integers 0-255 in a tuple).
        :return: the image with the raw path drawn.
        """

        # Define a color degradation convenience function.
        def degrade(this_color, this_ratio):
            degradation = (2 - this_ratio) / 2
            return tuple(map(lambda c: int(c * degradation), this_color))

        # Build color gradients from base color to indicate path direction (gradient is determined by normalized time).
        time_progression_ratios = self.points[:, 2] / self.lifespan
        colors = [degrade(color, ratio) for ratio in time_progression_ratios]

        # Draw raw path onto image.
        for x, y, color in zip(self.x_centroids, self.y_centroids, colors):
            image = cv2.circle(image, (int(x), int(y)), 6, color, -1)

        # Return image with raw path drawn.
        return image

    def draw_interp(self, image, color):
        """
        Draw this path on an input image using the interpolated path points (path featurized during call to __init__).

        :param image: image to draw this path on (using opencv).
        :param tuple color: a color spec (3 integers 0-255 in a tuple).
        :return: the image with the interpolated path drawn.
        """

        # Define a color degradation convenience function.
        def degrade(this_color, ratio):
            degradation = (2 - ratio) / 2
            return tuple(map(lambda c: int(c * degradation), this_color))

        # Build color gradients from base color to indicate path direction.
        num = len(self.points_interp)
        colors = [degrade(color, index / num) for index in range(num)]

        # Draw interpolated path onto image.
        for point, color in zip(self.points_interp, colors):
            image = cv2.circle(image, (int(point[0]), int(point[1])), 6, color, -1)

        # Return image with interpolated path drawn.
        return image

    def _featurize(self):
        try:
            tck, _ = interpolate.splprep([self.x_centroids, self.y_centroids], s=0)
            new_points = interpolate.splev(np.linspace(0, 1, Path._num_interpolations), tck)
            self.points_interp = np.zeros(shape=(Path._num_interpolations, 2))
            self.points_interp[:, 0] = new_points[0]
            self.points_interp[:, 1] = new_points[1]
        except:
            self.valid = False
            print(f"Failed to featurize path {self.id}, size {self.lifespan} {self.points.shape}")

    @property
    def centroids(self):
        return self.points[:, 0:1]

    @property
    def x_centroids(self):
        return self.points[:, 0]

    @property
    def y_centroids(self):
        return self.points[:, 1]

    @property
    def id(self):
        return self._id

    @classmethod
    def _generate_id(cls):
        cls._counter += 1
        return cls._counter

    @staticmethod
    def hausdorff(path1, path2, num_dimensions=2):
        """
        Calculate the maximized directed Hausdorf distance between two paths (using their centroids).
        Scipy docs: https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.directed_hausdorff.html.

        :param Path path1: first path.
        :param Path path2: second path.
        :param int num_dimensions: use 2 or 3 (with time) dimensions points for path parametrization.
        """
        if num_dimensions == 2:
            points1 = path1.centroids
            points2 = path2.centroids
        elif num_dimensions == 3:
            points1 = path1.points
            points2 = path2.points
        else:
            raise ValueError
        d1, _, _ = directed_hausdorff(u=points1, v=points2)
        d2, _, _ = directed_hausdorff(u=points2, v=points1)
        return max(d1, d2)

    @staticmethod
    def build_distance_matrix(paths):
        """
        Calculate the pair-wise distance matrix for the input list of paths.

        :param list paths: list of Path object to calculate distances for.
        :return: a symmetric square matrix where element i, j is the hausdorff distance between path_i and path_j.
        """
        distances = np.zeros(shape=(len(paths), len(paths)))
        for (index_i, path_i), (index_j, path_j) in itertools.combinations(enumerate(paths), 2):
            distances[index_i, index_j] = Path.hausdorff(path1=path_i, path2=path_j)
            distances[index_j, index_i] = distances[index_i, index_j]
        return distances

    @staticmethod
    def build_feature_matrix(paths):
        """
        Build a feature matrix for a set of paths using distance-based percentile spline path interpolation.

        :param list paths: list of Path object to build a feature matrix for.
        :return np.ndarray: a 2D array where the features of each path are flattened to a row: (n_paths, n_features).
        """
        X = np.zeros(shape=(len(paths), 2 * Path._num_interpolations))
        for index, path in enumerate(paths):
            assert path.valid
            X[index, :] = path.points_interp.reshape(1, -1)
        return X

    @staticmethod
    def draw_paths(paths, image):
        """
        Draw the collection of input paths on the input image.

        :param list paths: list of Path object to draw.
        :param image: image to draw the paths on (with opencv).
        """

        # Open a window.
        name = "window"
        cv2.namedWindow(name)
        cv2.resizeWindow(name, 400, 300)
        cv2.moveWindow(name, 500, 500)

        # Draw raw paths onto the image.
        for path in paths:
            image = path.draw_raw(image=image, color=(255, 0, 0))

        # Display the drawn paths on the image.
        cv2.imshow(name, image)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            return True
        cv2.destroyWindow(name)

    @staticmethod
    def draw_path_clusters(paths, labels, image, only_this_label=None):
        """
        Draw the input paths on the given image with colors associated to cluster labels.

        :param list paths: list of Path object to draw.
        :param labels: cluster labels for the input paths (-1 indicates no associated cluster).
        :param image: image to draw the paths on (with opencv).
        :param int only_this_label: if not None only the cluster associated with this label will be drawn.
        """
        # FIXME only_this_label=0 behaves like only_this_label=None.

        # Open a new window.
        name = "window"
        cv2.namedWindow(name)
        cv2.resizeWindow(name, 400, 300)
        cv2.moveWindow(name, 500, 500)

        # Associate colors with path clusters.
        random.seed(42)
        num_clusters = [label for label in np.unique(labels) if label != -1]
        colors = [(random.randint(0, 256), random.randint(0, 256), random.randint(0, 256)) for _ in range(len(num_clusters))]

        # Draw the interpolated paths of those paths that were associated with a cluster.
        for label, path in zip(labels, paths):
            if (not only_this_label and label == -1) or (only_this_label and label != only_this_label):
                continue
            image = path.draw_interp(image, color=colors[label])

        # Display the drawn paths on the image.
        cv2.imshow(name, image)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            return True
        cv2.destroyWindow(name)
