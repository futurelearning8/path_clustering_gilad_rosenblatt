import inspect
import numpy as np
from abc import ABC, abstractmethod
from scipy.optimize import linear_sum_assignment
from utils import TimerUtility, SyncIteratorWithCallbacks as SyncIteratorWithCallbacksMixin
from boxops import IOU, BoxMetric
from trackers import KalmanBoxTracker
from feeders import FeedIteratorWithCallbacks
from plotters import Plotter
from detectors import Detector


class MultiObjectTracker(SyncIteratorWithCallbacksMixin, ABC):
    """
    A multiple object tracker that listens to a feed iterator w/callbacks and plots frames with bounding boxes around
    tracked objects while the feed is active and performs cleanup once feed terminates. Implementing this class only
    requires to implement the _update method that updates the tracker bounding boxes given detector generated
    bounding boxes from current image (and a method to access those tracker bounding boxes).
    """

    def __init__(self, feed, detector, display_feed=False):
        """
        Register self to feed with the next frame callback function being the update method.

        :param FeedIteratorWithCallbacks feed: image feed iterator that allows to register listener callbacks.
        :param Detector detector: a detector that detects object in an image (has a detect method that returns boxes).
        :param Plotter plotter: a frame plotter.
        """

        # Save feed iterator.
        self._feed = feed

        # Save detector object.
        self._detector = detector

        # Save plotter object and set display feed status accordingly.
        self._display_feed = display_feed
        if self._display_feed:
            self._plotter = Plotter()
        else:
            self._plotter = None

        # Sync iterator with callbacks: register self as listener to get notified on get_frame events using _process_x.
        self.sync(iter_wc=feed, keys=["frame"])

    def _process_start(self):
        if self._display_feed:
            self._plotter.create_window(f"Feed name: {self._feed.name}")

    def _process_next(self, frame):

        # Do nothing if frame was incorrectly retrieved (e.g., reached end of video file). Avoid ambiguous truth values.
        if not isinstance(frame, np.ndarray) and frame is None:
            return

        # Detect objects in current frame.
        detection_boxes = self._detector.detect(frame)

        # Update trackers using their current state and this frame's detection bounding boxes.
        self._update(detection_boxes)

        # Get updated tracker bounding boxes.
        tracker_boxes = self.tracker_boxes

        # Draw new frame with id-ed tracker bounding boxes.
        if self._display_feed:

            # Load image corresponding to this frame.
            self._plotter.load_image(frame)

            # Draw tracker bounding boxes on this frame.
            self._plotter.draw_boxes(tracker_boxes)

            # Draw new frame on window (and kill feed quit keystroke 'q' was pressed).
            user_pressed_quit_keystroke = self._plotter.show_current_image()
            if user_pressed_quit_keystroke:
                self._feed.terminate()

    def _process_stop(self):
        if self._display_feed:
            self._plotter.destroy_window()

    def _write_to_file(self, output_file):
        """
        Write all tracker boxes to input file.

        :param output_file: file handler to output file.
        """
        for box in self.tracker_boxes:
            x1, y1, x2, y2, tracker_id = box
            w = x2 - x1  # Box width.
            h = y2 - y1  # Box height.
            frame_number = self._feed.index + 1
            print(f"{frame_number},{tracker_id},{x1:.2f},{y1:.2f},{w:.2f},{h:.2f},1,-1,-1,-1", file=output_file)

    def run(self, filename=None):
        """
        Run the multiple object tracker instance on the input feed by iteration the feed iterator w/callbacks,
        which implicitly updates trackers using listener mechanics (via _update). Optionally write tracker info to file.

        :param str filename: file path to which tracker boxes info will be written in each iteration.
        """
        print(f"Processing {self._feed.name}.")
        if filename:
            # Process feed inside a context manager.
            with open(filename, 'w') as output_file:
                for _ in self._feed:  # Calls _update at each iteration.
                    self._write_to_file(output_file=output_file)
        else:
            for _ in self._feed:  # Calls _update at each iteration.
                continue

    @property
    @abstractmethod
    def tracker_boxes(self):
        """Return all tracker bounding boxes for current frame."""
        pass

    @abstractmethod
    def _update(self, detection_boxes):
        """
        Update the state of the multi-object tracker by one frame (called from _process_next_frame).

        :param np.ndarray detection_boxes: all bounding boxes returned by the detector for current frame.
        """
        pass


class Sort(MultiObjectTracker):
    """Implements a Simple Online Realtime Tracking (SORT) multiple object tracker."""

    def __init__(self, max_age=1, min_hits=3, metric=IOU, *args, **kwargs):
        """
        Instantiate a SORT object tracker.

        :param StubFeed feed: iterator on detection boxes for each frame in sequence that allows listener callback.
        :param int max_age: maximum allowed age for valid tracker.
        :param int min_hits: minimum number of hits needed to consider a tracker as valid.
        :param BoxMetric metric: a class that implements the BoxMetric interface (default: intersection over union).
        """

        # Register self to feed with the next frame callback function being the self _update method.
        super(Sort, self).__init__(*args, **kwargs)

        # Save SORT object tracker settings.
        self._max_age = max_age
        self._min_hits = min_hits

        # Initialize trackers list.
        self._trackers = []

        # Initialize frame counter.
        self._frame_count = 0

        # Save metric callable (go through setter method).
        self.metric = metric

        # Initialize current frame tracker boxes to an empty numpy array (of appropriate format).
        self._tracker_boxes = np.empty((0, 5))

    @property
    def tracker_boxes(self):
        """
        :return numpy.ndarray: array of current matched tracker bounding boxes in the format:

             [[x1, y1, x2, y2, ID],  --> tracker box 1, tracker ID 1
              [x1, y1, x2, y2, ID],  --> tracker box 2, tracker ID 2
              ...                 ,
              [x1, y1, x2, y2, ID]]  --> tracker box n, tracker ID n
        """
        return self._tracker_boxes

    @property
    def max_age(self):
        return self._max_age

    @property
    def min_hits(self):
        return self._min_hits

    @property
    def metric(self):
        return self._metric

    @metric.setter
    def metric(self, metric):
        """
        Assert type for property metric.

        :param BoxMetric metric: a class that implements the BoxMetric interface (with static distance method).
        """
        assert inspect.isclass(metric) and issubclass(metric, BoxMetric)
        self._metric = metric

    @TimerUtility.cumulative_timeit  # Counts cumulative runtime spent in this method.
    def _update(self, detection_boxes):
        """
        Update the state of the SORT multi-object tracker by one frame.

        :param numpy.ndarray detection_boxes: array of detection bounding boxes in the format:

             [[x1, y1, x2, y2, score],  --> detection box 1, detection score 1
              [x1, y1, x2, y2, score],  --> detection box 2, detection score 2
              ...                    ,
              [x1, y1, x2, y2, score]]  --> detection box n, detection score n

        :return numpy.ndarray: array of matched (valid) tracker bounding boxes in the format:

             [[x1, y1, x2, y2, ID],  --> tracker box 1, tracker ID 1
              [x1, y1, x2, y2, ID],  --> tracker box 2, tracker ID 2
              ...                 ,
              [x1, y1, x2, y2, ID]]  --> tracker box m, tracker ID m

        NOTE: This method must be called once for each frame even if there are no detections.
        NOTE: The number of trackers returned may differ from the number of detections provided.
        """

        # Update frame counter for this frame.
        self._frame_count += 1

        # Instantiate tracker boxes array and auxiliary parameters.
        tracker_boxes = np.zeros((len(self._trackers), 5))
        invalid_tracker_indices = []
        valid_trackers = []

        # Predict tracker boxes for this frame using previous frame tracker box positions.
        for t_index, tracker_box in enumerate(tracker_boxes):

            # Predict new tracker box position and use slice assignment to alter tracker_boxes.
            box = self._trackers[t_index].predict()[0]
            tracker_box[:] = [box[0], box[1], box[2], box[3], 0]

            # For invalid predictions mark tracker index for deletion.
            if np.isnan(box).any():
                invalid_tracker_indices.append(t_index)

        # Discard trackers with NaN predictions (state change).
        tracker_boxes = np.ma.compress_rows(np.ma.masked_invalid(tracker_boxes))
        for t_index in reversed(invalid_tracker_indices):
            self._trackers.pop(t_index)

        # Match detection boxes to predicted tracker boxes using the provided metric.
        matches, unmatched_detections, unmatched_trackers = Sort._match_detections_to_trackers(
            detection_boxes,
            tracker_boxes,
            self._metric.distance
        )

        # Update matched trackers with assigned detections.
        for t_index, tracker in enumerate(self._trackers):
            if t_index not in unmatched_trackers:
                d = matches[np.where(matches[:, 1] == t_index)[0], 0]
                tracker.update(detection_boxes[d, :][0])

        # Create and initialize new trackers for unmatched detections.
        for d_index in unmatched_detections:
            tracker = KalmanBoxTracker(detection_boxes[d_index, :])
            self._trackers.append(tracker)

        # Iterate over all potentially valid trackers in reversed order.
        t_index = len(self._trackers)
        for tracker in reversed(self._trackers):
            t_index -= 1

            # Append valid trackers and their ID to output list in [box, ID + 1] row format.
            if tracker.time_since_update == 0 and (tracker.hit_streak >= self._min_hits
                                                   or self._frame_count <= self._min_hits):
                valid_trackers.append(np.concatenate((
                    tracker.box[0],
                    [tracker.id + 1]  # +1 as MOT benchmark requires positive values.
                )).reshape(1, -1))

            # Discard old trackers that have not been updated for a while.
            if tracker.time_since_update > self.max_age:
                self._trackers.pop(t_index)

        # Update current frame tracker boxes to valid tracker boxes and IDs.
        if not valid_trackers:
            self._tracker_boxes = np.empty((0, 5))
        else:
            self._tracker_boxes = np.concatenate(valid_trackers)

    @staticmethod
    def _match_detections_to_trackers(detection_boxes, tracker_boxes, metric_function, threshold=0.3):
        """
        Assign detections to tracked object using the Hungarian algorithm (both represented as bounding boxes).

        :param numpy.ndarray detection_boxes: bounding boxes matrix for detections in [*corners, score] row format.
        :param numpy.ndarray tracker_boxes: bounding boxes matrix for trackers in [*corners, ID] row format.
        :param metric_function: callable that takes 2 bounding boxes and returns a non-negative real number.
        :param float threshold: metric score threshold for detection-tracker match.
        :return: a tuple of 3 lists containing matches, unmatched_detections and unmatched_trackers.
        """

        # Extract number of trackers and detections
        num_detections = detection_boxes.shape[0]
        num_trackers = tracker_boxes.shape[0]

        # Return empty if no trackers are active.
        if num_trackers == 0:
            return np.empty((0, 2), dtype=int), \
                   np.arange(num_detections), \
                   np.empty((0, 5), dtype=int)

        # Instantiate metric score matrix.
        score_matrix = np.zeros((num_detections, num_trackers), dtype=np.float32)

        # Fill metric score matrix using the static distance method of the Metric interface.
        for d_index, d_box in enumerate(detection_boxes):
            for t_index, t_box in enumerate(tracker_boxes):
                score_matrix[d_index, t_index] = metric_function(d_box, t_box)

        # Match trackers to detections using an optimized scipy implementation of the Hungarian algorithm.
        matched_indices = np.array(linear_sum_assignment(-score_matrix)).T

        # List all unmatched detections.
        unmatched_detections = []
        for d_index, d_box in enumerate(detection_boxes):
            if d_index not in matched_indices[:, 0]:
                unmatched_detections.append(d_index)

        # List all unmatched trackers.
        unmatched_trackers = []
        for t_index, t_box in enumerate(tracker_boxes):
            if t_index not in matched_indices[:, 1]:
                unmatched_trackers.append(t_index)

        # Filter out detection-tracker matches with a low metric score (under the threshold).
        matches = []
        for match in matched_indices:
            if score_matrix[match[0], match[1]] < threshold:
                unmatched_detections.append(match[0])
                unmatched_trackers.append(match[1])
            else:
                matches.append(match.reshape(1, 2))

        # Convert list of matches to numpy array.
        if not matches:
            matches = np.empty((0, 2), dtype=int)
        else:
            matches = np.concatenate(matches, axis=0)

        # Return matches and un-matches as numpy arrays.
        return matches, np.array(unmatched_detections), np.array(unmatched_trackers)

    @staticmethod
    def print_report():
        """Print a statement about how much time was spend updating state (tracking with the _update method)."""
        print(
            f"Total Tracking took: {TimerUtility.total_time:.3f} "
            f"for {TimerUtility.total_count} frames "
            f"or {TimerUtility.total_count / TimerUtility.total_time:.1f} FPS."
        )
