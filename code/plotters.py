import cv2
import numpy as np


class Plotter:
    # Instance counter used as default window name.
    _window_id = 0

    # Default window-persistent random box coloring scheme.
    _colors = 255 * np.random.rand(32, 3)

    def __init__(self):
        self._window_id += 1  # Default window naming starts from 1.
        self._current_image = None
        self._window_name = None

    def create_window(self, name=None):
        if not name:
            self._window_name = f"Window {self._window_id}"
        else:
            self._window_name = name
        cv2.namedWindow(name)
        cv2.resizeWindow(name, 400, 300)
        cv2.moveWindow(name, 500, 500)

    def load_image(self, image):
        self._current_image = image

    def draw_boxes(self, boxes):
        """Iteratively draw bounding boxes on current image."""
        for box in boxes:
            # Unpack bounding box and its id.
            *box, box_id = box.astype(np.int32)

            # Draw box on current image and annotate with its ID.
            self.draw_box(box, box_id=box_id, color=self._pick_color(box_id))

    def draw_box(self, box, box_id, color):

        # Unpack box to corner pixels (1: upper left, 2: lower right).
        x1, y1, x2, y2 = box

        # Draw a bounding box (tracker state) onto this frame.
        self._current_image = cv2.rectangle(
            self._current_image,
            (x1, y1),  # Upper left corner.
            (x2, y2),  # Lower right corner.
            color=color,
            thickness=2
        )

        # Annotate each box with its ID (just above bounding box).
        cv2.putText(
            self._current_image,
            str(box_id),
            (int(x1), int(y1)),
            fontFace=cv2.FONT_HERSHEY_DUPLEX,
            fontScale=1,
            color=(255, 255, 255),
            lineType=2
        )

    def show_current_image(self):
        """
        Show current frame to window.

        :return bool: True is user pressed the quit keystroke ('q') False otherwise.
        """
        cv2.imshow(self._window_name, self._current_image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            return True
        return False  # Normal execution.

    def destroy_window(self):
        cv2.destroyWindow(self._window_name)
        self._window_name = None

    @property
    def window_name(self):
        return self._window_name

    @classmethod
    def _pick_color(cls, box_id):
        """
        Use window-persistent mapping to map ids to colors.

        :param int box_id: integer id to be mapped to a color.
        :return np.ndarray: an RGB color given by 3 integers in the format [0-255, 0-255, 0-255].
        """
        return cls._colors[box_id % 32, :]
