import cv2
import pandas as pd
import seaborn as sns
from sklearn.cluster import DBSCAN
from hdbscan import HDBSCAN
from paths import Path


def read_data():
    """
    Reads benchmark file and returns bounding box data for trackers in each frame.

    :return pd.DataFrame: tracker benchmarks in a data frame with centroid positions added to bounding box data.
    """
    df = pd.read_csv(
        filepath_or_buffer="../data/cluster/PETS09-S2L1-IOU-new-combined.txt",
        names=["frame_number", "tracker_id", "x1", "y1", "w", "h", "1", "2", "3", "4"],
        dtype={"frame_number": int, "tracker_id": int, "x1": float, "y1": float, "w": float, "h": float}
    ).drop(columns=["1", "2", "3", "4"])

    df["x_centroid"] = df["x1"] + df["w"] / 2.0
    df["y_centroid"] = df["y1"] + df["h"] / 2.0

    return df


def build_paths(df):
    """
    Build a collection of paths from tracker bounding box data. Only adds paths can be interpolated (3+ points).

    :param pd.DataFrame df: tracker benchmarks in a data frame with centroid positions of bounding boxes.
    :return list: list of Path object where each path corresponds to a unique tracker ID in the dataframe.
    """
    paths = []
    for tracker_id in df["tracker_id"].unique():
        this_path = Path(
            tracker_id=tracker_id,
            points=df[df["tracker_id"] == tracker_id][["x_centroid", "y_centroid", "frame_number"]].values
        )
        if this_path.valid:
            paths.append(this_path)
    return paths


def approach1(model_name="HDBSCAN"):
    """
    Cluster paths in the target video using a distance-matrix based clustering approach.

    :param str model_name: use a density-based scan ("DBSCAN") or hierarchical density-based scan ("HDBSCAN) model.
    """

    # Load paths from SORT multi-object tracker output on the target video and the first frame of the video.
    paths = build_paths(read_data())
    image = cv2.imread("../data/mot_benchmark/train/PETS09-S2L1/img1/000001.jpg")

    # Define the clustering model.
    if model_name == "HDBSCAN":
        model = HDBSCAN(
            metric="precomputed",
            memory="hdbscan_cache",
            min_cluster_size=4,
            min_samples=1
        )
    elif model_name == "DBSCAN":
        model = DBSCAN(
            metric="precomputed",
            eps=35,
            min_samples=2
        )  # 35, 2 on new (not combined)
    else:
        raise ValueError

    # Perform path clustering using the pair-wise Hausdorff distance matrix.
    distances = Path.build_distance_matrix(paths)
    model.fit(distances)
    # sns.heatmap(distances)

    # Print clustering groups to console.
    stats = pd.Series(model.labels_).value_counts()
    print(stats)

    # Draw paths on the first frame and display clustering result.
    # Path.draw_paths(paths=paths, image=image)
    Path.draw_path_clusters(paths=paths, labels=model.labels_, image=image)


def approach2(model_name="HDBSCAN"):
    """
    Cluster paths in the target video using a feature-matrix based clustering approach.

    :param str model_name: use a density-based scan ("DBSCAN") or hierarchical density-based scan ("HDBSCAN) model.
    """

    # Load paths from SORT multi-object tracker output on the target video and the first frame of the video.
    paths = build_paths(read_data())
    image = cv2.imread("../data/mot_benchmark/train/PETS09-S2L1/img1/000001.jpg")

    # Define the clustering model.
    if model_name == "HDBSCAN":
        model = HDBSCAN(
            min_cluster_size=6,
            min_samples=2
        )  # 3, 2 on IOU-new | 3, 1 $ 6, 2 on IOU-combined | 4, 1 on IOU-combined2
    elif model_name == "DBSCAN":
        model = DBSCAN(
            eps=400,
            min_samples=2
        )  # | 600, 2 on IOU-new |
    else:
        raise ValueError

    # Perform path clustering using the feature matrix.
    X = Path.build_feature_matrix(paths)
    model.fit(X)

    # Print clustering groups to console.
    stats = pd.Series(model.labels_).value_counts()
    print(stats / stats.sum())
    print(stats)

    # Draw paths on the first frame and display clustering result.
    # Path.draw_paths(paths=paths, image=image)
    Path.draw_path_clusters(paths=paths, labels=model.labels_, image=image)


if __name__ == "__main__":
    approach2()
