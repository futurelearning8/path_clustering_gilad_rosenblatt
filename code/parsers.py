import numpy as np


class DataParser:
    """Container of static methods to parse images and bounding boxes from mot_benchmark dataset files."""

    # Names of available training image sequences.
    train_sequences = [
        "PETS09-S2L1",
        "TUD-Campus",
        "TUD-Stadtmitte",
        "ETH-Bahnhof",
        "ETH-Sunnyday",
        "ETH-Pedcross2",
        "KITTI-13",
        "KITTI-17",
        "ADL-Rundle-6",
        "ADL-Rundle-8",
        "Venice-2"
    ]

    # Names of available test image sequences.
    test_sequences = [
        "ADL-Rundle-1",
        "ADL-Rundle-3",
        "AVG-TownCentre",
        "ETH-Crossing",
        "ETH-Jelmoli",
        "ETH-Linthescher",
        "KITTI-16",
        "KITTI-19",
        "PETS09-S2L2",
        "TUD-Crossing",
        "Venice-1"
    ]

    # Cached dictionary in the form {sequence_name: number_of_frames} (avoids reloading data file for each query).
    _number_of_frames = {}

    @staticmethod
    def exists(sequence):
        """
        Check if named sequence exists in the dataset.

        :param str sequence: name of image sequence.
        :return bool: indicates whether the named sequence exists in the dataset.
        """
        return True if sequence in DataParser.train_sequences + DataParser.test_sequences else False

    @staticmethod
    def get_phase(sequence):
        """
        Return whether the named sequence is part of the train or test set.

        :param str sequence: name of image sequence.
        :return str: indicates whether the named sequence belongs to the train or test partitions of the dataset.
        """
        return "train" if sequence in DataParser.train_sequences else "test"

    @staticmethod
    def get_base_path():
        """
        Return path to base folder for the mot benchmark dataset.

        :return str: path to folder containing the mot benchmark dataset.
        """
        return "../data/mot_benchmark"

    @staticmethod
    def _get_folder(sequence):
        """
        Return path to base folder for the named image sequence.

        :param str sequence: name of image sequence.
        :return str: path to folder containing data associated with the named sequence.
        """
        return f"{DataParser.get_base_path()}/{DataParser.get_phase(sequence)}/{sequence}"

    @staticmethod
    def get_filename(sequence, frame_number):
        """
        Return filename for the image file corresponding to given frame number in named image sequence.

        :param str sequence: name of image sequence.
        :param int frame_number: the number of frame in the sequence to get detection boxes for.
        :return str: path to image file for given frame number.
        """
        assert frame_number <= DataParser._number_of_frames[sequence]
        return f"{DataParser._get_folder(sequence)}/img1/{frame_number:06d}.jpg"

    @classmethod
    def get_detections(cls, sequence):
        """
        Read from file pre-detected bounding boxes for all framed in the named image sequence.

        :param str sequence: name of image sequence.
        :return np.ndarray: bounding boxes of pre-detected objects in the image sequence.
        """

        # Assert that sequence name is legal.
        assert DataParser.exists(sequence)

        # Load detection bounding boxes from file.
        filename = f"{DataParser._get_folder(sequence)}/det/det.txt"
        detections = np.loadtxt(filename, delimiter=',')

        # Cache number of frames (frame numbers given by left most column).
        cls._number_of_frames[sequence] = int(detections[:, 0].max())

        # Return array of detection bounding boxes.
        return detections

    @classmethod
    def get_number_of_frames(cls, sequence):
        """
        Read from file all pre-detected bounding boxes for the named image sequence.

        :param str sequence: name of image sequence.
        :return int: number of frames in the named image sequence.
        """

        # Assert that sequence name is legal.
        assert DataParser.exists(sequence)

        # Cache number of frames for this sequence if it is not already in cache.
        if sequence not in cls._number_of_frames:
            _ = cls.get_detections(sequence)

        # Return number of frames in sequence.
        return cls._number_of_frames[sequence]

    @staticmethod
    def extract_boxes(detections, frame_number):
        """
        Read detection boxes of given frame number from an array containing all bounding boxes in the image sequence.
        This method is meant to extract bounding boxes from the detections output of DataParser.get_detections by
        external objects that use the DataParser namespace to parse data from the dataset folder.

        :param np.ndarray detections: array of bounding boxes for objects detected in the the image sequence.
        :param int frame_number: the number of frame in the sequence to get detection boxes for.
        :return np.ndarray: array of bounding boxes for objects detected in frame number frame_number.
        """
        detection_boxes = detections[detections[:, 0] == frame_number, 2:7]
        detection_boxes[:, 2:4] += detection_boxes[:, 0:2]  # Convert [x1, y1, w, h] to [x1, y1, x2, y2].
        return detection_boxes


class VideoParser:
    """Container of static methods to parse raw video files from mot_benchmark dataset files."""

    # Names of available training video files (file download NOT available for <https://motchallenge.net/vis/KITTI-13>).
    train_sequences = [sequence for sequence in DataParser.train_sequences if sequence != "KITTI-13"]

    # Names of available test video files.
    test_sequences = DataParser.test_sequences

    @staticmethod
    def get_base_path():
        """
        Return path to base folder for the mot benchmark dataset raw video files.

        :return str: path to folder containing the mot benchmark dataset raw video files.
        """
        return "../data/videos"

    @staticmethod
    def get_filename(sequence):
        """
        Return filename for the raw video file corresponding to named image sequence.

        :param str sequence: name of image sequence.
        :return str: path to raw video file.
        """
        return f"{VideoParser.get_base_path()}/{DataParser.get_phase(sequence)}/{sequence}/{sequence}-raw.webm"


class OutputParser:
    """Container of static methods to parse output mot benchmark text files."""

    @staticmethod
    def get_base_path():
        """
        Return path to base folder for output benchmark text files.

        :return str: path to folder containing output benchmark text files.
        """
        return "../data/output"

    @staticmethod
    def get_filename(sequence):
        """
        Return filename for the output benchmark text file corresponding to named image sequence.

        :param str sequence: name of image sequence.
        :return str: path to output benchmark text file.
        """
        return f"{OutputParser.get_base_path()}/{sequence}.txt"
