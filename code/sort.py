"""
This code implements SORT: A Simple, Online and Realtime Tracker: <https://arxiv.org/pdf/1602.00763.pdf>.
It is an adaptation/refactor of code written by Alex Bewley (alex@dynamicdetection.com),
whose related (updated) repo can be found here: <https://github.com/abewley/sort/blob/master/sort.py>,
and is distributed under the GNU v3 license, which can be found here: <http://www.gnu.org/licenses/>.
Raw datasets can be found here: <https://motchallenge.net/data/MOT15/>.
------------------------------------------------------------------------------------------------------------------------
Re-implementation code written by Gilad Rosenblatt.
"""
import os.path
import argparse
from mots import Sort  # Use the SORT multi-object tracker on image feed (implemented in mots.py).
from boxops import IOU, Euclidean  # Allow several metrics for distance between two boxes (implemented in boxops.py).
from parsers import DataParser, VideoParser, OutputParser  # Implemented in parsers.py.
from feeders import FileFeed, StubFeed, VideoFeed  # Feed images using an iterator (implemented in feeders.py).
from detectors import StubDetector  # Implemented in detectors.py.


def main(display_demo=False, metric=IOU, raw_video=False):
    """Run multi-object tracking using SORT on "pre-detected" image sequences read from file and display results."""

    # Define set of image sequence names.
    if not raw_video:
        sequences = DataParser.train_sequences
    else:
        sequences = VideoParser.train_sequences

    # Iterate over sequences of images.
    for sequence in sequences:

        # Configure a feed to named image sequence.
        if display_demo and not raw_video:
            # Initialize an image file sequence feed that generates images.
            number_of_frames = DataParser.get_number_of_frames(sequence)
            feed = FileFeed(
                name=sequence,
                number_of_frames=number_of_frames,
                filenames=(DataParser.get_filename(sequence, index + 1) for index in range(number_of_frames))
            )
        elif display_demo and raw_video:
            # Initialize a raw video feed that generates images.
            feed = VideoFeed(
                capture_this=VideoParser.get_filename(sequence),
                name=sequence
            )
        else:
            # Initialize a file feed that only generates frame numbers (not the actual images).
            feed = StubFeed(
                name=sequence,
                number_of_frames=DataParser.get_number_of_frames(sequence)
            )

        # Create a generator for detection boxes as input to a stub detector.
        # FIXME: Bounding boxes read by StubDetector are out of sync with frames generated by VideoFeed for 3 sequences.
        number_of_frames = DataParser.get_number_of_frames(sequence)
        detections = DataParser.get_detections(sequence)
        boxes = (DataParser.extract_boxes(detections, frame_number=index + 1) for index in range(number_of_frames))
        detector = StubDetector(boxes_generator=boxes)

        # Instantiate a new multiple object tracker and set its box distance metric.
        mo_tracker = Sort(
            feed=feed,  # MOT is locked to an image feed and updates implicitly each time get_frame is called.
            detector=detector,  # Detector used to extract bounding boxes for each frame generated by feed.
            display_feed=display_demo,  # Toggle output graphics.
            max_age=1,
            min_hits=3
        )
        mo_tracker.metric = metric

        # Run multiple object tracker on feed to process sequence (until feed is terminated/exhausted).
        mo_tracker.run(OutputParser.get_filename(sequence))

    # Print performance report.
    Sort.print_report()
    if display_demo:
        print("NOTE: To get real runtime results run without the --display flag.")


def parse_args():
    """Parse input arguments (look for the --display flag)."""
    parser = argparse.ArgumentParser(description='SORT demo')
    parser.add_argument(
        '--display',
        dest='display',
        help='Display online tracker output (slow) [False]',
        action='store_true'
    )
    return parser.parse_args()


if __name__ == "__main__":

    # Get display flag status from input arguments.
    args = parse_args()
    display_demo = args.display

    # Exit if path to the image data does not exist and print a download link to the dataset.
    if display_demo and not os.path.exists(DataParser.get_base_path()):
        print(
            "\n\tERROR: mot_benchmark link not found!\n\n"
            "    Create a symbolic link to the MOT benchmark\n"
            "    (https://motchallenge.net/data/MOT15/). E.g.:\n\n"  # Updated link to dataset.
            "    $ ln -s /path/to/MOT2015_challenge/2DMOT2015 ../data/mot_benchmark\n\n"  # Updated target directory.
        )
        exit()

    # Create a folder to store outputs if none exists.
    if not os.path.exists(OutputParser.get_base_path()):
        os.makedirs(OutputParser.get_base_path())

    # Run multiple object detection. Can choose metric IOU or Euclidean. Can choose to read from images or raw video.
    main(display_demo, metric=IOU, raw_video=False)
